package com.cqeec;

import com.cqeec.util.Field;
import com.cqeec.util.GenerateXmlData;
import com.cqeec.util.MyUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class MainTest {

    public static void main(String[] args) throws Exception {
         System.out.println("请输入需要用到的表名称并以逗号分隔");
         String tables=new Scanner(System.in).next();
         String[] tableList=tables.split(",");
        //#######
        //# 第一步edu_employ_results,edu_member,edu_employ_report
        //#######
        System.out.println("###1");
         int i=1;
         StringBuffer queryField=new StringBuffer();
         List<Field> info=new ArrayList<>();
         for(String table:tableList){
             List<Field> list=MyUtil.getFieldsByTableName(table);
             String tableName=getTableName(i);
             i++;
             System.out.println("请选择需要使用到的字段并以逗号分隔");
             int j=0;
             for(Field field:list){
                 if(j%5==0) System.out.println();
                 System.out.print(j+"、"+field.getDatabaseName()+"     ");
                 j++;
             }
             String temp1=new Scanner(System.in).next();
             String[] temp2=temp1.split(",");
             List<String> nums=Arrays.asList(temp2);
             j=0;
             for(Field field:list){
                 if(nums.contains(""+j)){
                     queryField.append(tableName+"."+field.getDatabaseName()+"   "+"as"+"   "+field.getJavaName()+",\n");
                     info.add(field);
                 }
                 j++;
             }
         }
        String data="select"+"\n"+queryField.substring(0,queryField.length()-2)+"\nfrom ";
        //#######
        //# 第二步
        //#######
        System.out.println("###2");
        i=1;
        for(String table:tableList){
            String tableName=getTableName(i);
             if(i==1){
             data+=table+" as a\n ";
             }else{
                 System.out.println("请输入连接方式:");
                 data+=new Scanner(System.in).next()+" "+table+" as "+tableName+"\n";
                 System.out.println("请输入连接条件:");
                 String condition=new Scanner(System.in).nextLine();
                 data+=condition;
             }
             i++;
        }

        //#######
        //# 第三步
        //#######
        System.out.println("###3");
        System.out.println("请选择需要的条件:");
        i=1;
        List<Field> condition=new ArrayList<>();
        for(String table:tableList){
            List<Field> list=MyUtil.getFieldsByTableName(table);
            String tableName=getTableName(i);
            i++;
            System.out.println("请选择需要使用到的字段并以逗号分隔");
            int j=0;
            for(Field field:list){
                if(j%5==0) System.out.println();
                System.out.print(j+"、"+field.getDatabaseName()+"     ");
                j++;
            }
            String temp1=new Scanner(System.in).next();
            String[] temp2=temp1.split(",");
            List<String> nums=Arrays.asList(temp2);
            j=0;
            for(Field field:list){
                if(nums.contains(""+j)){
                    field.setTableAsName(tableName);
                    condition.add(field);
                }
                j++;
            }
        }
        GenerateXmlData generateXmlData=new GenerateXmlData();
        generateXmlData.setConditions(condition);
        generateXmlData.setInfo(info);
        generateXmlData.setSql(data);
        generateXmlData.setSimpleName(MyUtil.covertTableName(tableList[0]));
        System.out.println(MyUtil.generateXml(generateXmlData));
        System.out.println(MyUtil.generateInfo(generateXmlData));
    }

    private static String getTableName(int i) {
        String tableName="";
        if(i==1){
            tableName="a";
        }else if(i==2){
            tableName="b";
        }else if(i==3){
            tableName="c";
        }else if(i==4){
            tableName="d";
        }else if(i==5){
            tableName="f";
        }
        return tableName;
    }

}
