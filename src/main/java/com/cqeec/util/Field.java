package com.cqeec.util;

import lombok.Data;

/**
 * 孤独的灯塔<br/>
 * date:2020/1/7<br/>
 *
 * @Author:hxh 1361973421@qq.com
 */
@Data
public class Field {
    private String javaName;
    private String databaseName;
    private String databaseType;
    private String javaType;
    private String comment;
    private String tableAsName;


}
