package com.cqeec.util;

public enum JavaTypeEnums {
    VARCHAR("VARCHAR"),
    INTEGER("INTEGER"),
    DECIMAL("DECIMAL"),
    LONG("LONG"),
    DOUBLE("DOUBLE"),
    TIMESTAMP("TIMESTAMP");

    /**
     * 枚举值
     */
    private final String code;

    private JavaTypeEnums(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
