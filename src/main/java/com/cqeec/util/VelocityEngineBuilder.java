package com.cqeec.util;

import com.google.common.base.Supplier;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

public class VelocityEngineBuilder {

    private VelocityEngine velocityEngine;


    private VelocityEngineBuilder(VelocityEngine velocityEngine) {
        this.velocityEngine = velocityEngine;
    }

    public static VelocityEngineBuilder startWithDefault() {
        VelocityEngine ve = new VelocityEngine();
        // 设置资源路径
        ve.setProperty(org.apache.velocity.runtime.RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        // 初始化
        ve.init();
        return new VelocityEngineBuilder(ve);
    }

    public VelocityTemplate withTemplatePath(Supplier<String> resourcePath) {
        Template template = velocityEngine.getTemplate(resourcePath.get());

        return new VelocityTemplate(velocityEngine, template);
    }


    public static class VelocityTemplate {
        private VelocityEngine velocityEngine;
        private Template template;

        public VelocityTemplate(VelocityEngine velocityEngine, Template template) {
            this.velocityEngine = velocityEngine;
            this.template = template;
        }

        public VelocityContext withContextBuild() {
            org.apache.velocity.VelocityContext ctx = new org.apache.velocity.VelocityContext();
            return new VelocityContext(template, ctx);
        }
    }


    public static class VelocityContext {
        private Template template;
        private org.apache.velocity.VelocityContext context;

        public VelocityContext(Template template, org.apache
            .velocity.VelocityContext context) {
            this.template = template;
            this.context = context;
        }

        public VelocityContext setKV(String k, Object v) {
            context.put(k, v);
            return this;
        }

        public String getString(){
            StringWriter sw = new StringWriter();
            template.merge(context, sw);
            String result = sw.toString();
            return result;
        }

    }


}

