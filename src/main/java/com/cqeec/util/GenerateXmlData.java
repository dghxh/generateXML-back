package com.cqeec.util;

import lombok.Data;

import java.util.List;

@Data
public class GenerateXmlData {
    //Field集合
    List<Field> info;
    //sql标签，封装条件
    List<Field> conditions;
    //需要查询的sql语句
    String sql;
    //表的简单类名
    String simpleName;


}
